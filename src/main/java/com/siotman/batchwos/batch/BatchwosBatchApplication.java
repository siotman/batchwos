package com.siotman.batchwos.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatchwosBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(BatchwosBatchApplication.class, args);
	}

}
