package com.siotman.batchwos.parent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatchwosParentApplication {

    public static void main(String[] args) {
        SpringApplication.run(BatchwosParentApplication.class, args);
    }

}
